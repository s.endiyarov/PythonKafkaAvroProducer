from kafkaSchemaManager.decorator.CommandCenterFactory import CommandCenterFactory
from kafkaSchemaManager.decorator.commandCenterEnum import CommandCenterEnum
from kafkaSchemaManager.implementation.config.JsonKafkaConfig import JsonKafkaConfig
from kafkaSchemaManager.implementation.helpers.JsonFileSaver import JsonFileSaver
from kafkaSchemaManager.implementation.localSchema.JsonLocalSchemaLoader import JsonLocalSchemaLoader
from kafkaSchemaManager.implementation.localSchema.LocalSchemaHolder import LocalSchemaHolder
from kafkaSchemaManager.implementation.schema.AvroSchema import AvroSchema
from kafkaSchemaManager.implementation.config.postgreSqlDevConfig import PostgreSqlDevConfig
from kafkaSchemaManager.implementation.postgreBootstrap.postgreSqlOperations import PostgreSqlOperations
from kafkaSchemaManager.implementation.localSchema.SqlPostgreLocalSchemaSaver import SqlPostgreLocalSchemaSaver

import json, uuid

#set topic
topicName = "raw_data"

postgreOperations = PostgreSqlOperations(PostgreSqlDevConfig())
postgreOperations.openDatabase()   
localSchemaSaver = SqlPostgreLocalSchemaSaver(topicName,postgreOperations)

#create local schema JSON file from Raw JSON schema
jsonLoader = JsonFileSaver("latestSchemaRawData.json",topicName)
rawSchema = jsonLoader.load()
avroSchema = AvroSchema(None,None).assignObject(rawSchema)
schemaHolder = LocalSchemaHolder(True,topicName,avroSchema)
localSchemaSaver.save(schemaHolder)



