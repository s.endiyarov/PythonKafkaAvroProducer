import json
import os.path
import decimal
import datetime
import six
from avrogen.dict_wrapper import DictWrapper
from avrogen import avrojson
from avro import schema as avro_schema
if six.PY3:    from avro.schema import SchemaFromJSONData as make_avsc_object
    
else:
    from avro.schema import make_avsc_object
    
_STRING_SCHEMA_JSON = "{\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"Advertisement\", \"fields\": [{\"name\": \"uuid\", \"type\": [\"null\", \"string\"]}, {\"name\": \"type\", \"type\": [\"null\", \"string\"]}, {\"name\": \"source\", \"type\": [\"null\", \"string\"]}, {\"name\": \"url\", \"type\": [\"null\", \"string\"]}, {\"name\": \"description\", \"type\": [\"null\", \"string\"]}, {\"name\": \"created_at\", \"type\": [\"null\", \"string\"]}, {\"name\": \"updated_at\", \"type\": [\"null\", \"string\"]}, {\"name\": \"geo\", \"type\": [\"null\", {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"Geo\", \"fields\": [{\"name\": \"owner_uri\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"raw_address\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"country\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"region\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"city\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"zip\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"address_line\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"lat\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"lon\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"meta\", \"type\": [\"null\", \"string\"], \"default\": null}]}], \"default\": null}, {\"name\": \"media_files\", \"type\": {\"type\": \"array\", \"items\": {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"MediaFiles\", \"fields\": [{\"name\": \"source\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"type\", \"type\": [\"null\", \"string\"], \"default\": null}]}}, \"default\": []}, {\"name\": \"object\", \"type\": [\"null\", {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"Object\", \"fields\": [{\"name\": \"type\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"business_offered\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"current_use\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"possible_use\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"object_data\", \"type\": [\"null\", {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"ObjectData\", \"fields\": [{\"name\": \"type\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"building\", \"type\": [\"null\", {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"Building\", \"fields\": [{\"name\": \"type\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"year_of_built\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"category\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"num_of_storeys\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"current_use\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"building_material\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"emergency_state\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"rating_of_condition\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"gross_area\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"net_area\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"property_header\", \"type\": [\"null\", {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"PropertyHeader\", \"fields\": [{\"name\": \"obj_header\", \"type\": [\"null\", {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"ObjectHeader\", \"fields\": [{\"name\": \"id\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"obj_uuid\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"obj_cat\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"obt_type\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"root_obj_uuid\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"parent_obj_uuid\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"child_obj_uuid\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"obj_uri\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"added_at\", \"type\": [\"null\", \"string\"], \"default\": null}]}], \"default\": null}, {\"name\": \"short_summary\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"geo_postring\", \"type\": [\"null\", \"com.topkrabbensteam.zm.rawdatastream.Geo\"], \"default\": null}, {\"name\": \"pledgor\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"owner\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"owners_count\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"object_class\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"type\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"cadastral_number\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"rights\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"encumbrances\", \"type\": [\"null\", \"string\"], \"default\": null}]}], \"default\": null}]}], \"default\": null}, {\"name\": \"premises\", \"type\": [\"null\", {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"Premises\", \"fields\": [{\"name\": \"property_header\", \"type\": [\"null\", \"com.topkrabbensteam.zm.rawdatastream.PropertyHeader\"], \"default\": null}, {\"name\": \"cadastral_value\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"cadastral_currency\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"purpose_of_the_premise\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"current_use\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"gross_area\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"net_area\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"occupied_area\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"area_type\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"separate_entrance\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"backyard_entrance\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"parking_type\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"finishing_quality\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"high_secondary_street\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"display_windows\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"floor\", \"type\": [\"null\", {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"Floor\", \"fields\": [{\"name\": \"level\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"is_last\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"type\", \"type\": [\"null\", \"string\"], \"default\": null}]}], \"default\": null}, {\"name\": \"meta\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"number_of_floors\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"year_of_construction\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"end_date_of_rental_period\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"wall_material\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"physical_condition_of_the_object\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"composition_of_rental_rate\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"proximity_to_the_bus_stop\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"average_throughput_per_day\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"access_to_object\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"photos\", \"type\": {\"type\": \"array\", \"items\": {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"PhotoObject\", \"fields\": [{\"name\": \"made_date\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"photo_name\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"photo_base64\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"inspector_id\", \"type\": [\"null\", \"string\"], \"default\": null}]}}, \"default\": []}]}], \"default\": null}]}], \"default\": null}]}], \"default\": null}, {\"name\": \"prices\", \"type\": {\"type\": \"array\", \"items\": {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"Prices\", \"fields\": [{\"name\": \"value\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"type\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"type_comment\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"value_type\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"value_type_comment\", \"type\": [\"null\", \"string\"], \"default\": null}]}}, \"default\": []}, {\"name\": \"owner\", \"type\": [\"null\", {\"type\": \"record\", \"namespace\": \"com.topkrabbensteam.zm.rawdatastream\", \"name\": \"Owner\", \"fields\": [{\"name\": \"name\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"phone\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"email\", \"type\": [\"null\", \"string\"], \"default\": null}, {\"name\": \"site\", \"type\": [\"null\", \"string\"], \"default\": null}]}], \"default\": null}]}"



def __read_file(file_name):
    with open(file_name, "r") as f:
        return f.read()

def __get_names_and_schema():
    names = avro_schema.Names()
    schema = make_avsc_object(json.loads(_STRING_SCHEMA_JSON), names)
    return names, schema

__NAMES, SCHEMA = __get_names_and_schema()
__SCHEMAS = {}
def get_schema_type(fullname):
    return __SCHEMAS.get(fullname)
__SCHEMAS = dict((n.fullname.lstrip("."), n) for n in six.itervalues(__NAMES.names))


class SchemaClasses(object):
    
    
    pass
    class com(object):
        class topkrabbensteam(object):
            class zm(object):
                class rawdatastream(object):
                    
                    class AdvertisementClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.Advertisement")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.AdvertisementClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.uuid = None
                                self.type = None
                                self.source = None
                                self.url = None
                                self.description = None
                                self.created_at = None
                                self.updated_at = None
                                self.geo = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.AdvertisementClass.RECORD_SCHEMA.fields[7].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.AdvertisementClass.RECORD_SCHEMA.fields[7].type))
                                self.media_files = list()
                                self.object = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.AdvertisementClass.RECORD_SCHEMA.fields[9].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.AdvertisementClass.RECORD_SCHEMA.fields[9].type))
                                self.prices = list()
                                self.owner = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.AdvertisementClass.RECORD_SCHEMA.fields[11].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.AdvertisementClass.RECORD_SCHEMA.fields[11].type))
                        
                        
                        @property
                        def uuid(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('uuid')
                        
                        @uuid.setter
                        def uuid(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['uuid'] = value
                        
                        
                        @property
                        def type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('type')
                        
                        @type.setter
                        def type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['type'] = value
                        
                        
                        @property
                        def source(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('source')
                        
                        @source.setter
                        def source(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['source'] = value
                        
                        
                        @property
                        def url(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('url')
                        
                        @url.setter
                        def url(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['url'] = value
                        
                        
                        @property
                        def description(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('description')
                        
                        @description.setter
                        def description(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['description'] = value
                        
                        
                        @property
                        def created_at(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('created_at')
                        
                        @created_at.setter
                        def created_at(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['created_at'] = value
                        
                        
                        @property
                        def updated_at(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('updated_at')
                        
                        @updated_at.setter
                        def updated_at(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['updated_at'] = value
                        
                        
                        @property
                        def geo(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass
                            """
                            return self._inner_dict.get('geo')
                        
                        @geo.setter
                        def geo(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass value:
                            #"""
                            self._inner_dict['geo'] = value
                        
                        
                        @property
                        def media_files(self):
                            """
                            :rtype: list[SchemaClasses.com.topkrabbensteam.zm.rawdatastream.MediaFilesClass]
                            """
                            return self._inner_dict.get('media_files')
                        
                        @media_files.setter
                        def media_files(self, value):
                            #"""
                            #:param list[SchemaClasses.com.topkrabbensteam.zm.rawdatastream.MediaFilesClass] value:
                            #"""
                            self._inner_dict['media_files'] = value
                        
                        
                        @property
                        def object(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass
                            """
                            return self._inner_dict.get('object')
                        
                        @object.setter
                        def object(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass value:
                            #"""
                            self._inner_dict['object'] = value
                        
                        
                        @property
                        def prices(self):
                            """
                            :rtype: list[SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass]
                            """
                            return self._inner_dict.get('prices')
                        
                        @prices.setter
                        def prices(self, value):
                            #"""
                            #:param list[SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass] value:
                            #"""
                            self._inner_dict['prices'] = value
                        
                        
                        @property
                        def owner(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass
                            """
                            return self._inner_dict.get('owner')
                        
                        @owner.setter
                        def owner(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass value:
                            #"""
                            self._inner_dict['owner'] = value
                        
                        
                    class BuildingClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.Building")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[0].default
                                self.year_of_built = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[1].default
                                self.category = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[2].default
                                self.num_of_storeys = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[3].default
                                self.current_use = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[4].default
                                self.building_material = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[5].default
                                self.emergency_state = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[6].default
                                self.rating_of_condition = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[7].default
                                self.gross_area = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[8].default
                                self.net_area = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[9].default
                                self.property_header = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[10].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass.RECORD_SCHEMA.fields[10].type))
                        
                        
                        @property
                        def type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('type')
                        
                        @type.setter
                        def type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['type'] = value
                        
                        
                        @property
                        def year_of_built(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('year_of_built')
                        
                        @year_of_built.setter
                        def year_of_built(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['year_of_built'] = value
                        
                        
                        @property
                        def category(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('category')
                        
                        @category.setter
                        def category(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['category'] = value
                        
                        
                        @property
                        def num_of_storeys(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('num_of_storeys')
                        
                        @num_of_storeys.setter
                        def num_of_storeys(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['num_of_storeys'] = value
                        
                        
                        @property
                        def current_use(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('current_use')
                        
                        @current_use.setter
                        def current_use(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['current_use'] = value
                        
                        
                        @property
                        def building_material(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('building_material')
                        
                        @building_material.setter
                        def building_material(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['building_material'] = value
                        
                        
                        @property
                        def emergency_state(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('emergency_state')
                        
                        @emergency_state.setter
                        def emergency_state(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['emergency_state'] = value
                        
                        
                        @property
                        def rating_of_condition(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('rating_of_condition')
                        
                        @rating_of_condition.setter
                        def rating_of_condition(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['rating_of_condition'] = value
                        
                        
                        @property
                        def gross_area(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('gross_area')
                        
                        @gross_area.setter
                        def gross_area(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['gross_area'] = value
                        
                        
                        @property
                        def net_area(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('net_area')
                        
                        @net_area.setter
                        def net_area(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['net_area'] = value
                        
                        
                        @property
                        def property_header(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass
                            """
                            return self._inner_dict.get('property_header')
                        
                        @property_header.setter
                        def property_header(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass value:
                            #"""
                            self._inner_dict['property_header'] = value
                        
                        
                    class FloorClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.Floor")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.FloorClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.level = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.FloorClass.RECORD_SCHEMA.fields[0].default
                                self.is_last = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.FloorClass.RECORD_SCHEMA.fields[1].default
                                self.type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.FloorClass.RECORD_SCHEMA.fields[2].default
                        
                        
                        @property
                        def level(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('level')
                        
                        @level.setter
                        def level(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['level'] = value
                        
                        
                        @property
                        def is_last(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('is_last')
                        
                        @is_last.setter
                        def is_last(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['is_last'] = value
                        
                        
                        @property
                        def type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('type')
                        
                        @type.setter
                        def type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['type'] = value
                        
                        
                    class GeoClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.Geo")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.owner_uri = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass.RECORD_SCHEMA.fields[0].default
                                self.raw_address = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass.RECORD_SCHEMA.fields[1].default
                                self.country = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass.RECORD_SCHEMA.fields[2].default
                                self.region = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass.RECORD_SCHEMA.fields[3].default
                                self.city = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass.RECORD_SCHEMA.fields[4].default
                                self.zip = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass.RECORD_SCHEMA.fields[5].default
                                self.address_line = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass.RECORD_SCHEMA.fields[6].default
                                self.lat = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass.RECORD_SCHEMA.fields[7].default
                                self.lon = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass.RECORD_SCHEMA.fields[8].default
                                self.meta = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass.RECORD_SCHEMA.fields[9].default
                        
                        
                        @property
                        def owner_uri(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('owner_uri')
                        
                        @owner_uri.setter
                        def owner_uri(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['owner_uri'] = value
                        
                        
                        @property
                        def raw_address(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('raw_address')
                        
                        @raw_address.setter
                        def raw_address(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['raw_address'] = value
                        
                        
                        @property
                        def country(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('country')
                        
                        @country.setter
                        def country(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['country'] = value
                        
                        
                        @property
                        def region(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('region')
                        
                        @region.setter
                        def region(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['region'] = value
                        
                        
                        @property
                        def city(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('city')
                        
                        @city.setter
                        def city(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['city'] = value
                        
                        
                        @property
                        def zip(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('zip')
                        
                        @zip.setter
                        def zip(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['zip'] = value
                        
                        
                        @property
                        def address_line(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('address_line')
                        
                        @address_line.setter
                        def address_line(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['address_line'] = value
                        
                        
                        @property
                        def lat(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('lat')
                        
                        @lat.setter
                        def lat(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['lat'] = value
                        
                        
                        @property
                        def lon(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('lon')
                        
                        @lon.setter
                        def lon(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['lon'] = value
                        
                        
                        @property
                        def meta(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('meta')
                        
                        @meta.setter
                        def meta(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['meta'] = value
                        
                        
                    class MediaFilesClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.MediaFiles")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.MediaFilesClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.source = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.MediaFilesClass.RECORD_SCHEMA.fields[0].default
                                self.type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.MediaFilesClass.RECORD_SCHEMA.fields[1].default
                        
                        
                        @property
                        def source(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('source')
                        
                        @source.setter
                        def source(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['source'] = value
                        
                        
                        @property
                        def type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('type')
                        
                        @type.setter
                        def type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['type'] = value
                        
                        
                    class ObjectClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.Object")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass.RECORD_SCHEMA.fields[0].default
                                self.business_offered = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass.RECORD_SCHEMA.fields[1].default
                                self.current_use = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass.RECORD_SCHEMA.fields[2].default
                                self.possible_use = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass.RECORD_SCHEMA.fields[3].default
                                self.object_data = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass.RECORD_SCHEMA.fields[4].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass.RECORD_SCHEMA.fields[4].type))
                        
                        
                        @property
                        def type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('type')
                        
                        @type.setter
                        def type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['type'] = value
                        
                        
                        @property
                        def business_offered(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('business_offered')
                        
                        @business_offered.setter
                        def business_offered(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['business_offered'] = value
                        
                        
                        @property
                        def current_use(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('current_use')
                        
                        @current_use.setter
                        def current_use(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['current_use'] = value
                        
                        
                        @property
                        def possible_use(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('possible_use')
                        
                        @possible_use.setter
                        def possible_use(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['possible_use'] = value
                        
                        
                        @property
                        def object_data(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass
                            """
                            return self._inner_dict.get('object_data')
                        
                        @object_data.setter
                        def object_data(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass value:
                            #"""
                            self._inner_dict['object_data'] = value
                        
                        
                    class ObjectDataClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.ObjectData")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass.RECORD_SCHEMA.fields[0].default
                                self.building = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass.RECORD_SCHEMA.fields[1].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass.RECORD_SCHEMA.fields[1].type))
                                self.premises = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass.RECORD_SCHEMA.fields[2].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass.RECORD_SCHEMA.fields[2].type))
                        
                        
                        @property
                        def type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('type')
                        
                        @type.setter
                        def type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['type'] = value
                        
                        
                        @property
                        def building(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass
                            """
                            return self._inner_dict.get('building')
                        
                        @building.setter
                        def building(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass value:
                            #"""
                            self._inner_dict['building'] = value
                        
                        
                        @property
                        def premises(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass
                            """
                            return self._inner_dict.get('premises')
                        
                        @premises.setter
                        def premises(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass value:
                            #"""
                            self._inner_dict['premises'] = value
                        
                        
                    class ObjectHeaderClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.ObjectHeader")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.id = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass.RECORD_SCHEMA.fields[0].default
                                self.obj_uuid = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass.RECORD_SCHEMA.fields[1].default
                                self.obj_cat = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass.RECORD_SCHEMA.fields[2].default
                                self.obt_type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass.RECORD_SCHEMA.fields[3].default
                                self.root_obj_uuid = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass.RECORD_SCHEMA.fields[4].default
                                self.parent_obj_uuid = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass.RECORD_SCHEMA.fields[5].default
                                self.child_obj_uuid = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass.RECORD_SCHEMA.fields[6].default
                                self.obj_uri = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass.RECORD_SCHEMA.fields[7].default
                                self.added_at = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass.RECORD_SCHEMA.fields[8].default
                        
                        
                        @property
                        def id(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('id')
                        
                        @id.setter
                        def id(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['id'] = value
                        
                        
                        @property
                        def obj_uuid(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('obj_uuid')
                        
                        @obj_uuid.setter
                        def obj_uuid(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['obj_uuid'] = value
                        
                        
                        @property
                        def obj_cat(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('obj_cat')
                        
                        @obj_cat.setter
                        def obj_cat(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['obj_cat'] = value
                        
                        
                        @property
                        def obt_type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('obt_type')
                        
                        @obt_type.setter
                        def obt_type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['obt_type'] = value
                        
                        
                        @property
                        def root_obj_uuid(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('root_obj_uuid')
                        
                        @root_obj_uuid.setter
                        def root_obj_uuid(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['root_obj_uuid'] = value
                        
                        
                        @property
                        def parent_obj_uuid(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('parent_obj_uuid')
                        
                        @parent_obj_uuid.setter
                        def parent_obj_uuid(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['parent_obj_uuid'] = value
                        
                        
                        @property
                        def child_obj_uuid(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('child_obj_uuid')
                        
                        @child_obj_uuid.setter
                        def child_obj_uuid(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['child_obj_uuid'] = value
                        
                        
                        @property
                        def obj_uri(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('obj_uri')
                        
                        @obj_uri.setter
                        def obj_uri(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['obj_uri'] = value
                        
                        
                        @property
                        def added_at(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('added_at')
                        
                        @added_at.setter
                        def added_at(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['added_at'] = value
                        
                        
                    class OwnerClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.Owner")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.name = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass.RECORD_SCHEMA.fields[0].default
                                self.phone = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass.RECORD_SCHEMA.fields[1].default
                                self.email = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass.RECORD_SCHEMA.fields[2].default
                                self.site = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass.RECORD_SCHEMA.fields[3].default
                        
                        
                        @property
                        def name(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('name')
                        
                        @name.setter
                        def name(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['name'] = value
                        
                        
                        @property
                        def phone(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('phone')
                        
                        @phone.setter
                        def phone(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['phone'] = value
                        
                        
                        @property
                        def email(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('email')
                        
                        @email.setter
                        def email(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['email'] = value
                        
                        
                        @property
                        def site(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('site')
                        
                        @site.setter
                        def site(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['site'] = value
                        
                        
                    class PhotoObjectClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.PhotoObject")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PhotoObjectClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.made_date = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PhotoObjectClass.RECORD_SCHEMA.fields[0].default
                                self.photo_name = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PhotoObjectClass.RECORD_SCHEMA.fields[1].default
                                self.photo_base64 = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PhotoObjectClass.RECORD_SCHEMA.fields[2].default
                                self.inspector_id = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PhotoObjectClass.RECORD_SCHEMA.fields[3].default
                        
                        
                        @property
                        def made_date(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('made_date')
                        
                        @made_date.setter
                        def made_date(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['made_date'] = value
                        
                        
                        @property
                        def photo_name(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('photo_name')
                        
                        @photo_name.setter
                        def photo_name(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['photo_name'] = value
                        
                        
                        @property
                        def photo_base64(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('photo_base64')
                        
                        @photo_base64.setter
                        def photo_base64(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['photo_base64'] = value
                        
                        
                        @property
                        def inspector_id(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('inspector_id')
                        
                        @inspector_id.setter
                        def inspector_id(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['inspector_id'] = value
                        
                        
                    class PremisesClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.Premises")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.property_header = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[0].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[0].type))
                                self.cadastral_value = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[1].default
                                self.cadastral_currency = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[2].default
                                self.purpose_of_the_premise = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[3].default
                                self.current_use = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[4].default
                                self.gross_area = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[5].default
                                self.net_area = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[6].default
                                self.occupied_area = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[7].default
                                self.area_type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[8].default
                                self.separate_entrance = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[9].default
                                self.backyard_entrance = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[10].default
                                self.parking_type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[11].default
                                self.finishing_quality = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[12].default
                                self.high_secondary_street = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[13].default
                                self.display_windows = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[14].default
                                self.floor = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.FloorClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[15].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[15].type))
                                self.meta = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[16].default
                                self.number_of_floors = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[17].default
                                self.year_of_construction = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[18].default
                                self.end_date_of_rental_period = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[19].default
                                self.wall_material = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[20].default
                                self.physical_condition_of_the_object = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[21].default
                                self.composition_of_rental_rate = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[22].default
                                self.proximity_to_the_bus_stop = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[23].default
                                self.average_throughput_per_day = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[24].default
                                self.access_to_object = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass.RECORD_SCHEMA.fields[25].default
                                self.photos = list()
                        
                        
                        @property
                        def property_header(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass
                            """
                            return self._inner_dict.get('property_header')
                        
                        @property_header.setter
                        def property_header(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass value:
                            #"""
                            self._inner_dict['property_header'] = value
                        
                        
                        @property
                        def cadastral_value(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('cadastral_value')
                        
                        @cadastral_value.setter
                        def cadastral_value(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['cadastral_value'] = value
                        
                        
                        @property
                        def cadastral_currency(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('cadastral_currency')
                        
                        @cadastral_currency.setter
                        def cadastral_currency(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['cadastral_currency'] = value
                        
                        
                        @property
                        def purpose_of_the_premise(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('purpose_of_the_premise')
                        
                        @purpose_of_the_premise.setter
                        def purpose_of_the_premise(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['purpose_of_the_premise'] = value
                        
                        
                        @property
                        def current_use(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('current_use')
                        
                        @current_use.setter
                        def current_use(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['current_use'] = value
                        
                        
                        @property
                        def gross_area(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('gross_area')
                        
                        @gross_area.setter
                        def gross_area(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['gross_area'] = value
                        
                        
                        @property
                        def net_area(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('net_area')
                        
                        @net_area.setter
                        def net_area(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['net_area'] = value
                        
                        
                        @property
                        def occupied_area(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('occupied_area')
                        
                        @occupied_area.setter
                        def occupied_area(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['occupied_area'] = value
                        
                        
                        @property
                        def area_type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('area_type')
                        
                        @area_type.setter
                        def area_type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['area_type'] = value
                        
                        
                        @property
                        def separate_entrance(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('separate_entrance')
                        
                        @separate_entrance.setter
                        def separate_entrance(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['separate_entrance'] = value
                        
                        
                        @property
                        def backyard_entrance(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('backyard_entrance')
                        
                        @backyard_entrance.setter
                        def backyard_entrance(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['backyard_entrance'] = value
                        
                        
                        @property
                        def parking_type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('parking_type')
                        
                        @parking_type.setter
                        def parking_type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['parking_type'] = value
                        
                        
                        @property
                        def finishing_quality(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('finishing_quality')
                        
                        @finishing_quality.setter
                        def finishing_quality(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['finishing_quality'] = value
                        
                        
                        @property
                        def high_secondary_street(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('high_secondary_street')
                        
                        @high_secondary_street.setter
                        def high_secondary_street(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['high_secondary_street'] = value
                        
                        
                        @property
                        def display_windows(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('display_windows')
                        
                        @display_windows.setter
                        def display_windows(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['display_windows'] = value
                        
                        
                        @property
                        def floor(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.FloorClass
                            """
                            return self._inner_dict.get('floor')
                        
                        @floor.setter
                        def floor(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.FloorClass value:
                            #"""
                            self._inner_dict['floor'] = value
                        
                        
                        @property
                        def meta(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('meta')
                        
                        @meta.setter
                        def meta(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['meta'] = value
                        
                        
                        @property
                        def number_of_floors(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('number_of_floors')
                        
                        @number_of_floors.setter
                        def number_of_floors(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['number_of_floors'] = value
                        
                        
                        @property
                        def year_of_construction(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('year_of_construction')
                        
                        @year_of_construction.setter
                        def year_of_construction(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['year_of_construction'] = value
                        
                        
                        @property
                        def end_date_of_rental_period(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('end_date_of_rental_period')
                        
                        @end_date_of_rental_period.setter
                        def end_date_of_rental_period(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['end_date_of_rental_period'] = value
                        
                        
                        @property
                        def wall_material(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('wall_material')
                        
                        @wall_material.setter
                        def wall_material(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['wall_material'] = value
                        
                        
                        @property
                        def physical_condition_of_the_object(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('physical_condition_of_the_object')
                        
                        @physical_condition_of_the_object.setter
                        def physical_condition_of_the_object(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['physical_condition_of_the_object'] = value
                        
                        
                        @property
                        def composition_of_rental_rate(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('composition_of_rental_rate')
                        
                        @composition_of_rental_rate.setter
                        def composition_of_rental_rate(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['composition_of_rental_rate'] = value
                        
                        
                        @property
                        def proximity_to_the_bus_stop(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('proximity_to_the_bus_stop')
                        
                        @proximity_to_the_bus_stop.setter
                        def proximity_to_the_bus_stop(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['proximity_to_the_bus_stop'] = value
                        
                        
                        @property
                        def average_throughput_per_day(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('average_throughput_per_day')
                        
                        @average_throughput_per_day.setter
                        def average_throughput_per_day(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['average_throughput_per_day'] = value
                        
                        
                        @property
                        def access_to_object(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('access_to_object')
                        
                        @access_to_object.setter
                        def access_to_object(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['access_to_object'] = value
                        
                        
                        @property
                        def photos(self):
                            """
                            :rtype: list[SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PhotoObjectClass]
                            """
                            return self._inner_dict.get('photos')
                        
                        @photos.setter
                        def photos(self, value):
                            #"""
                            #:param list[SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PhotoObjectClass] value:
                            #"""
                            self._inner_dict['photos'] = value
                        
                        
                    class PricesClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.Prices")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.value = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass.RECORD_SCHEMA.fields[0].default
                                self.type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass.RECORD_SCHEMA.fields[1].default
                                self.type_comment = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass.RECORD_SCHEMA.fields[2].default
                                self.value_type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass.RECORD_SCHEMA.fields[3].default
                                self.value_type_comment = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass.RECORD_SCHEMA.fields[4].default
                        
                        
                        @property
                        def value(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('value')
                        
                        @value.setter
                        def value(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['value'] = value
                        
                        
                        @property
                        def type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('type')
                        
                        @type.setter
                        def type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['type'] = value
                        
                        
                        @property
                        def type_comment(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('type_comment')
                        
                        @type_comment.setter
                        def type_comment(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['type_comment'] = value
                        
                        
                        @property
                        def value_type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('value_type')
                        
                        @value_type.setter
                        def value_type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['value_type'] = value
                        
                        
                        @property
                        def value_type_comment(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('value_type_comment')
                        
                        @value_type_comment.setter
                        def value_type_comment(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['value_type_comment'] = value
                        
                        
                    class PropertyHeaderClass(DictWrapper):
                        
                        """
                        
                        """
                        
                        
                        RECORD_SCHEMA = get_schema_type("com.topkrabbensteam.zm.rawdatastream.PropertyHeader")
                        
                        
                        def __init__(self, inner_dict=None):
                            super(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass, self).__init__(inner_dict)
                            if inner_dict is None:
                                self.obj_header = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[0].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[0].type))
                                self.short_summary = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[1].default
                                self.geo_postring = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass(_json_converter.from_json_object(SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[2].default, writers_schema=SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[2].type))
                                self.pledgor = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[3].default
                                self.owner = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[4].default
                                self.owners_count = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[5].default
                                self.object_class = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[6].default
                                self.type = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[7].default
                                self.cadastral_number = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[8].default
                                self.rights = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[9].default
                                self.encumbrances = SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass.RECORD_SCHEMA.fields[10].default
                        
                        
                        @property
                        def obj_header(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass
                            """
                            return self._inner_dict.get('obj_header')
                        
                        @obj_header.setter
                        def obj_header(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass value:
                            #"""
                            self._inner_dict['obj_header'] = value
                        
                        
                        @property
                        def short_summary(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('short_summary')
                        
                        @short_summary.setter
                        def short_summary(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['short_summary'] = value
                        
                        
                        @property
                        def geo_postring(self):
                            """
                            :rtype: SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass
                            """
                            return self._inner_dict.get('geo_postring')
                        
                        @geo_postring.setter
                        def geo_postring(self, value):
                            #"""
                            #:param SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass value:
                            #"""
                            self._inner_dict['geo_postring'] = value
                        
                        
                        @property
                        def pledgor(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('pledgor')
                        
                        @pledgor.setter
                        def pledgor(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['pledgor'] = value
                        
                        
                        @property
                        def owner(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('owner')
                        
                        @owner.setter
                        def owner(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['owner'] = value
                        
                        
                        @property
                        def owners_count(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('owners_count')
                        
                        @owners_count.setter
                        def owners_count(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['owners_count'] = value
                        
                        
                        @property
                        def object_class(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('object_class')
                        
                        @object_class.setter
                        def object_class(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['object_class'] = value
                        
                        
                        @property
                        def type(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('type')
                        
                        @type.setter
                        def type(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['type'] = value
                        
                        
                        @property
                        def cadastral_number(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('cadastral_number')
                        
                        @cadastral_number.setter
                        def cadastral_number(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['cadastral_number'] = value
                        
                        
                        @property
                        def rights(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('rights')
                        
                        @rights.setter
                        def rights(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['rights'] = value
                        
                        
                        @property
                        def encumbrances(self):
                            """
                            :rtype: str
                            """
                            return self._inner_dict.get('encumbrances')
                        
                        @encumbrances.setter
                        def encumbrances(self, value):
                            #"""
                            #:param str value:
                            #"""
                            self._inner_dict['encumbrances'] = value
                        
                        
                    pass
                    
__SCHEMA_TYPES = {
'com.topkrabbensteam.zm.rawdatastream.Advertisement': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.AdvertisementClass,
    'com.topkrabbensteam.zm.rawdatastream.Building': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.BuildingClass,
    'com.topkrabbensteam.zm.rawdatastream.Floor': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.FloorClass,
    'com.topkrabbensteam.zm.rawdatastream.Geo': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.GeoClass,
    'com.topkrabbensteam.zm.rawdatastream.MediaFiles': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.MediaFilesClass,
    'com.topkrabbensteam.zm.rawdatastream.Object': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectClass,
    'com.topkrabbensteam.zm.rawdatastream.ObjectData': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectDataClass,
    'com.topkrabbensteam.zm.rawdatastream.ObjectHeader': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.ObjectHeaderClass,
    'com.topkrabbensteam.zm.rawdatastream.Owner': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.OwnerClass,
    'com.topkrabbensteam.zm.rawdatastream.PhotoObject': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PhotoObjectClass,
    'com.topkrabbensteam.zm.rawdatastream.Premises': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PremisesClass,
    'com.topkrabbensteam.zm.rawdatastream.Prices': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PricesClass,
    'com.topkrabbensteam.zm.rawdatastream.PropertyHeader': SchemaClasses.com.topkrabbensteam.zm.rawdatastream.PropertyHeaderClass,
    
}
_json_converter = avrojson.AvroJsonConverter(use_logical_types=False, schema_types=__SCHEMA_TYPES)

