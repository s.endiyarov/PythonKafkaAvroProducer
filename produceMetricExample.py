from kafkaSchemaManager.decorator.CommandCenterFactory import CommandCenterFactory
from kafkaSchemaManager.decorator.commandCenterEnum import CommandCenterEnum
from kafkaSchemaManager.implementation.postgreBootstrap.parserKafkaSchemaHolder import ParserKafkaSchemaHolder
from kafkaSchemaManager.implementation.postgreBootstrap.postgreSqlOperations import PostgreSqlOperations
from kafkaSchemaManager.implementation.config.postgreSqlLocalConfig import PostgreSqlLocalConfig
from kafkaSchemaManager.implementation.config.JsonKafkaConfig import JsonKafkaConfig

from kafkaSchemaManager.implementation.schemaMigration.schemaCreation import CreateSchema
from kafkaSchemaManager.implementation.localSchema.JsonLocalSchemaLoader import JsonLocalSchemaLoader

import random
import time
from metricsFacade.kafkaMetricsSchema.com.topkrabbensteam.zm.custom_metric.enums import PrometheusMetricType
from metricsFacade.kafkaMetricsSchema.com.topkrabbensteam.zm.metrics.metric_header import  MetricHeader
from metricsFacade.kafkaMetricsSchema.com.topkrabbensteam.zm.metrics.prometheus_metric import  PrometheusMetricData
from metricsFacade.kafkaMetricsSchema.com.topkrabbensteam.zm.metrics import MetricsList
from metricsFacade.kafkaMetricsSchema.com.topkrabbensteam.zm.metrics import Metric
from metricsFacade.kafkaMetricsSchema.com.topkrabbensteam.zm.metrics.prometheus_metric import LabelMap


def GetMetricType(index):
    if index  == 0:
        return PrometheusMetricType.Gauge
    if index == 1:
        return PrometheusMetricType.Counter
    if index == 2:
        return PrometheusMetricType.Histogram
    if index == 3:
        return PrometheusMetricType.Summary


def GetMetricName(prometherusMetricType):
    if prometherusMetricType == PrometheusMetricType.Counter:
        return "processed_adverts"
    if prometherusMetricType == PrometheusMetricType.Gauge:
        return "filled_field_count"
    if prometherusMetricType == PrometheusMetricType.Histogram:
        return "filled_field_historgam"
    if prometherusMetricType == PrometheusMetricType.Summary:
        return "filled_field_summary"


current_milli_time = lambda: int(round(time.time() * 1000))

topicName = "metrics_data"

commandCenter = CommandCenterFactory.createCommandCenter(CommandCenterEnum.LocalKafkaStorageCommandCenter,
                                                         topicName,
                                                         JsonKafkaConfig("config.json"),
                                                         None, 
                                                         None)

metricsData = MetricsList()
metric = Metric()
metric.data_type = "RED"

metricHeader = MetricHeader()
metricHeader.requester_uri = "some requested URI"
metricHeader.pushed_at = current_milli_time()
metricHeader.source = "some source"
metricHeader.source_type = "Backend"
metricHeader.priority = "Middle"
metricHeader.level = "INFO"
metric.metric_header = metricHeader

while True:
    time.sleep(5)
    metricValue = (random.random()*100)

    flag = (metricValue > (random.random()*100))
    flag1 = (metricValue > (random.random()*100))
    flag2 = (metricValue > (random.random()*100))

    siteLabelValue = "cian.ru" if flag else "drom.ru"
    platformLabelValue = "datacol" if not flag2 else "scrappy"
    moduleLabelValue =  "processingModule" if not flag1 else "parserModule"    

    #create metric
    prometheusMetric = PrometheusMetricData()
        
    prometheusMetric.metric_type = GetMetricType(random.randint(0,3))

    prometheusMetric.metric_name = GetMetricName(prometheusMetric.metric_type)

    if (prometheusMetric.metric_type == PrometheusMetricType.Histogram):
        prometheusMetric.buckets = [0,20,40,50,60,70,80,90,100,float('Inf')]
    else:
        prometheusMetric.buckets = []


    print ('Producing metric: %(type)s %(site)s %(platform)s %(module)s %(value)f' 
           % {"site": siteLabelValue,"platform": platformLabelValue,"module":moduleLabelValue,"value":metricValue,"type":prometheusMetric.metric_type})

    #set metric value
    prometheusMetric.metric_value = metricValue

    #add labels
    metricLabel = LabelMap()
    metricLabel.label = "site"
    metricLabel.lable_value = siteLabelValue
    prometheusMetric.metric_labels.append(metricLabel)

    metricLabel = LabelMap()
    metricLabel.label = "parserFramework"
    metricLabel.lable_value = platformLabelValue
    prometheusMetric.metric_labels.append(metricLabel)

    metricLabel = LabelMap()
    metricLabel.label = "module"
    metricLabel.lable_value = moduleLabelValue
    prometheusMetric.metric_labels.append(metricLabel)

    metric.prometheus_metric_data = prometheusMetric

    metricsData.metrics.append(metric)

    commandCenter.ProduceKafkaMessage(metricsData)

    metricsData.metrics.clear();


                                                         



