# PythonKafkaAvroProducer

# Dependencies:

* 1) pip install avro-python3==1.8.2;
* 2) pip install datamountaineer-schemaregistry==0.3;
* 3) pip install kafka-python==1.4.3;
* 4) pip install fastavro==0.14.11
* 5) pip install psycopg2==2.7.5
* 6) pip install SQLAlchemy==1.2.10

# Usage:

## Implement config classes based on AbstractKafkaConfig, AbstractDatabaseConfig
* AbstractDatabaseConfig - stands for database connection config
* AbstractKafkaConfig - stands for kafka connection config

## Select one of the following possibilities:

 * LocalStorageCommandCenter = 0, #stands for managing Avro Schema from local JSON file
 
 * PostgreeSqlCommandCenter = 1, #managing Avro Schema based on PostgreSQL database
 
 * LocalKafkaStorageCommandCenter = 2 #do not able to change schema, but can get it from Kafka and produce messages based on the present Kafaka schema
 
```python
from kafkaSchemaManager.decorator.CommandCenterFactory import CommandCenterFactory
from kafkaSchemaManager.decorator.commandCenterEnum import CommandCenterEnum

## Create command center
commandCenter = CommandCenterFactory.createCommandCenter(CommandCenterEnum.PostgreeSqlCommandCenter,
                                                         "testTopic",
                                                         JsonKafkaConfig("config.json"),
                                                         PostgreSqlLocalConfig(), 
                                                         None)

## Produce Avro message to Kafka
commandCenter.ProduceKafkaMessage({"Email":"seregae@e1.ru","ParserName": "must have", "myNewBrandField2": "Hello world"})

## Change Kafka schema runtime

#add a new field to a Kafka schema
commandCenter.AddNullStringSchemaField("jedi00087","my new command field")
#update it locally and on server
commandCenter.UpdateLocalSchemaAndThenUpdateServerKafkaSchema()
					
## Update local schema from JSON file
localSchemaJson = JsonLocalSchemaLoader("localMigrationTopic.json","testTopic").loadLocalSchema()
commandCenter.localSchemaHolder = localSchemaJson
commandCenter.UpdateLocalSchema()					
														 
## Check local schema (json base, database based etc.) and present Kafka schema

commandCenter.GetKafkaSchemaOperations().checkIfSchemaExists(schemaName)
commandCenter.IsKafkaSchemaIsOutdated()
														 
```

#Env creation via conda
conda create -n iskKing2 python=3.6.6

activate iskKing2

pip install --no-cache-dir -r requirements.txt