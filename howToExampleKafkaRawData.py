
#kafka components

from kafkaSchemaManager.decorator.CommandCenterFactory import CommandCenterFactory
from kafkaSchemaManager.decorator.commandCenterEnum import CommandCenterEnum
from kafkaSchemaManager.implementation.config.JsonKafkaConfig import JsonKafkaConfig
from kafkaSchemaManager.implementation.helpers.JsonFileSaver import JsonFileSaver
from kafkaSchemaManager.implementation.localSchema.LocalSchemaHolder import LocalSchemaHolder
from kafkaSchemaManager.implementation.schema.AvroSchema import AvroSchema
from kafkaSchemaManager.implementation.schemaObjectGeneration.objectsGenerator import ObjectsGenerator

#kafka schema details
from kafkaRawSchema import my_schema
from avro import datafile, io
from kafkaRawSchema.com.topkrabbensteam.zm.rawdatastream import Advertisement,Geo,MediaFiles,Object,ObjectData,Owner,Prices, \
Building,Floor,ObjectHeader,PhotoObject,Premises,PropertyHeader


def countRawDataFilledFields(object):
    total_filled_fields = 0
    for attr, value in object.items():
        if value is not None:
            if type(value) is list:
                for i in range(0,len(value)):
                    total_filled_fields = total_filled_fields + countRawDataFilledFields(value[i])
            elif type(value) is not str:
                total_filled_fields = total_filled_fields + countRawDataFilledFields(value)
            else:
                total_filled_fields = total_filled_fields + 1
    return total_filled_fields


import json, uuid

#set topic
topicSchemaName = "raw_data"

#create local schema JSON file from Raw JSON schema
jsonLoader = JsonFileSaver("latestSchemaRawData.json",topicSchemaName)
rawSchema = jsonLoader.load()
avroSchema = AvroSchema(None,None).assignObject(rawSchema)
schemaHolder = LocalSchemaHolder(True,topicSchemaName,avroSchema)
jsonLoader.path = "latestLocalSchema.json"
jsonLoader.save(schemaHolder)

#generate objects according to schema
objectsGenerator = ObjectsGenerator("kafkaRawSchema",json.dumps(rawSchema))
objectsGenerator.generateObjects()

#init command center from local storage
commandCenter = CommandCenterFactory.createCommandCenter(CommandCenterEnum.LocalStorageCommandCenter,
                                                         topicSchemaName,
                                                         JsonKafkaConfig("config.json"),
                                                         None, 
                                                         "latestLocalSchema.json")


#update kafka schema
commandCenter.UpdateLocalSchemaAndThenUpdateServerKafkaSchema()                                                         


#create sample object acctording to a schema
advert = Advertisement()
advert.uuid = "123123-123123-234234"
advert.type = "1"
advert.source = "cian"
advert.url = "http://cian.ru"
advert.description = "Hello"
advert.created_at = "10.02.2018"

advert.owner.name = "Ivan"
advert.owner.email = "Ivan@e1.ru"
advert.owner.phone = "32167111"

advert.object.type = "1"
advert.object.possible_use ="no use at all"
advert.object.business_offered = "False"

mediaFile = MediaFiles()
mediaFile.type = "1"
mediaFile.source = "http:\\hello.jpg"
advert.media_files.append(mediaFile)

prices = Prices()
prices.type = "1"
prices.type_comment = "type 1"
prices.value = "123445.0"
prices.value_type = "3"
advert.prices.append(prices)

advert.object.object_data.type = "5"
advert.object.object_data.building.property_header.obj_header.obj_uri ="some uri"
advert.object.object_data.premises.area_type = "3"
advert.object.object_data.premises.cadastral_value = "334567.3"
advert.object.object_data.premises.floor.is_last = "0"
advert.object.object_data.premises.floor.level = "5"

inspectionPhoto = PhotoObject()
inspectionPhoto.inspector_id = "565"
inspectionPhoto.photo_name = "photoTitle"
advert.object.object_data.premises.photos.append(inspectionPhoto)

inspectionPhoto = PhotoObject()
inspectionPhoto.inspector_id = "565"
inspectionPhoto.photo_name = "My new Photo Object"
advert.object.object_data.premises.photos.append(inspectionPhoto)

inspectionPhoto = PhotoObject()
inspectionPhoto.inspector_id = "777"
inspectionPhoto.photo_name = "My new Photo Object"
advert.object.object_data.premises.photos.append(inspectionPhoto)


total_fileld_fields  =  countRawDataFilledFields(advert)
print("Total filled fields %(total)s " % {"total": total_fileld_fields})



#produce object
commandCenter.ProduceKafkaMessage(advert)

print("Hey fist message was sent to Kafka")

#produce second time using schema id this time
advert.geo.city = "Novosibirsk city"

for i in range(0,100):
    advert.uuid =  str(uuid.uuid4()) +  "_myNewId_" + str(i)
    commandCenter.ProduceKafkaMessage(advert)


print("Hey second message was sent to Kafka")