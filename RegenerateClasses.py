#kafka components

from kafkaSchemaManager.decorator.CommandCenterFactory import CommandCenterFactory
from kafkaSchemaManager.decorator.commandCenterEnum import CommandCenterEnum
from kafkaSchemaManager.implementation.config.JsonKafkaConfig import JsonKafkaConfig
from kafkaSchemaManager.implementation.helpers.JsonFileSaver import JsonFileSaver
from kafkaSchemaManager.implementation.localSchema.LocalSchemaHolder import LocalSchemaHolder
from kafkaSchemaManager.implementation.schema.AvroSchema import AvroSchema
from kafkaSchemaManager.implementation.schemaObjectGeneration.objectsGenerator import ObjectsGenerator

#kafka schema details
from kafkaRawSchema import my_schema
from avro import datafile, io
from kafkaRawSchema.com.topkrabbensteam.zm.rawdatastream import Advertisement,Geo,MediaFiles,Object,ObjectData,Owner,Prices, \
Building,Floor,ObjectHeader,PhotoObject,Premises,PropertyHeader

import json

#set topic
topicSchemaName = "raw_data"

#create local schema JSON file from Raw JSON schema
jsonLoader = JsonFileSaver("latestSchemaRawData.json",topicSchemaName)
rawSchema = jsonLoader.load()

#generate objects according to schema
objectsGenerator = ObjectsGenerator("kafkaRawSchema",json.dumps(rawSchema))
objectsGenerator.generateObjects()


topicSchemaName = "metrics_data"

#create local schema JSON file from Raw JSON schema
jsonLoader = JsonFileSaver("metricsSchema.json",topicSchemaName)
rawSchema = jsonLoader.load()

#generate objects according to schema
objectsGenerator = ObjectsGenerator("metricsFacade/kafkaMetricsSchema",json.dumps(rawSchema))
objectsGenerator.generateObjects()

