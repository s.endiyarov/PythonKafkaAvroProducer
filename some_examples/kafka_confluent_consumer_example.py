
import sys
import json

from confluent_kafka import KafkaError
from confluent_kafka.avro import AvroConsumer
from confluent_kafka.avro.serializer import SerializerError


# need pip install confluent-kafka
# pip install confluent-kafka[avro]

c = AvroConsumer({
    'bootstrap.servers': '172.17.217.108:6667',
    'group.id': 'groupid',
    'schema.registry.url': 'http://172.17.217.108:8083'})

c.subscribe(['processed_data'])

while True:
    try:
        msg = c.poll(3)

    except SerializerError as _err:
        sys.stdout.write("Message deserialization failed for {}: {}\n".format(msg, e))
        break

    if not msg:
        continue

    if msg.error():
        if msg.error().code() == KafkaError._PARTITION_EOF:
            continue
        else:
            sys.stdout.write('%s\n' % msg.error())
            break

    sys.stdout.write('{}\n'.format(json.dumps(msg.value(), indent=4, sort_keys=True, ensure_ascii=False)))

c.close()