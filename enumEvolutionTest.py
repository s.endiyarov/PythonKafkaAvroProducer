
#kafka components
from kafkaSchemaManager.decorator.CommandCenterFactory import CommandCenterFactory
from kafkaSchemaManager.decorator.commandCenterEnum import CommandCenterEnum
from kafkaSchemaManager.implementation.config.JsonKafkaConfig import JsonKafkaConfig
from kafkaSchemaManager.implementation.helpers.JsonFileSaver import JsonFileSaver
from kafkaSchemaManager.implementation.localSchema.LocalSchemaHolder import LocalSchemaHolder
from kafkaSchemaManager.implementation.schema.AvroSchema import AvroSchema

#kafka schema details
from kafkaSchema import my_schema
from avro import datafile, io
from kafkaSchema.com.topkrabbensteam.zm.rawdatastream import Advertisement,Geo,MediaFiles,Object,ObjectData,Owner,Prices, \
Building,Floor,ObjectHeader,PhotoObject,Premises,PropertyHeader

from enum import Enum
import json

#set topic
topicSchemaName = "enumBest"

#create local schema JSON file from Raw JSON schema
jsonLoader = JsonFileSaver("enumSchemaTest.json",topicSchemaName)
rawSchema = jsonLoader.load()
avroSchema = AvroSchema(None,None).assignObject(rawSchema)
schemaHolder = LocalSchemaHolder(True,topicSchemaName,avroSchema)
jsonLoader.path = "enumTestLocalSchema.json"
jsonLoader.save(schemaHolder)

#init command center from local storage
commandCenter = CommandCenterFactory.createCommandCenter(CommandCenterEnum.LocalStorageCommandCenter,
                                                         topicSchemaName,
                                                         JsonKafkaConfig("config.json"),
                                                         None, 
                                                         "enumTestLocalSchema.json")
#update kafka schema
commandCenter.UpdateLocalSchemaAndThenUpdateServerKafkaSchema()     

#enumMy = KafkaEnum.B
commandCenter.ProduceKafkaMessage({"category":"OptionF"})
