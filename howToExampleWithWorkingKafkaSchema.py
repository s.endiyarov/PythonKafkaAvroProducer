
#kafka components
from kafkaSchemaManager.decorator.CommandCenterFactory import CommandCenterFactory
from kafkaSchemaManager.decorator.commandCenterEnum import CommandCenterEnum
from kafkaSchemaManager.implementation.config.JsonKafkaConfig import JsonKafkaConfig
from kafkaSchemaManager.implementation.helpers.JsonFileSaver import JsonFileSaver
from kafkaSchemaManager.implementation.localSchema.LocalSchemaHolder import LocalSchemaHolder
from kafkaSchemaManager.implementation.schema.AvroSchema import AvroSchema

#kafka schema details
from kafkaSchema import my_schema
from avro import datafile, io
from kafkaSchema.com.topkrabbensteam.zm.rawdatastream import Advertisement,Geo,MediaFiles,Object,ObjectData,Owner,Prices, \
Building,Floor,ObjectHeader,PhotoObject,Premises,PropertyHeader

import json

#set topic
topicSchemaName = "parsedDataUpdated3"


#create local schema JSON file from Raw JSON schema
jsonLoader = JsonFileSaver("latestSchemaMod.json",topicSchemaName)
rawSchema = jsonLoader.load()
avroSchema = AvroSchema(None,None).assignObject(rawSchema)
schemaHolder = LocalSchemaHolder(True,topicSchemaName,avroSchema)
jsonLoader.path = "latestLocalSchema.json"
jsonLoader.save(schemaHolder)



#init command center from local storage
commandCenter = CommandCenterFactory.createCommandCenter(CommandCenterEnum.LocalStorageCommandCenter,
                                                         topicSchemaName,
                                                         JsonKafkaConfig("config.json"),
                                                         None, 
                                                         "latestLocalSchema.json")


#update kafka schema
commandCenter.UpdateLocalSchemaAndThenUpdateServerKafkaSchema()                                                         


#create sample object acctording to a schema
advert = Advertisement()
advert.uuid = "123123-123123-234234"
advert.type = 1
advert.source = "cian"
advert.url = "http://cian.ru"

advert.owner.name = "Ivan"
advert.owner.email = "Ivan@e1.ru"
advert.owner.phone = "32167111"

advert.object.type = 1
advert.object.possible_use ="no use at all"
advert.object.business_offered = False

mediaFile = MediaFiles()
mediaFile.type = 1
mediaFile.source = "http:\\hello.jpg"
advert.media_files.append(mediaFile)

prices = Prices()
prices.type = 1
prices.type_comment = "type 1"
prices.value = 123445.0
prices.value_type = 3
advert.prices.append(prices)

advert.object.object_data.type = 5
advert.object.object_data.building.property_header.obj_header.obj_uri ="some uri"
advert.object.object_data.premises.area_type = 3
advert.object.object_data.premises.cadastral_value = 334567.3
advert.object.object_data.premises.floor.is_last = 0
advert.object.object_data.premises.floor.level = 5

inspectionPhoto = PhotoObject()
inspectionPhoto.inspector_id = 565
inspectionPhoto.photo_name = "photoTitle"
advert.object.object_data.premises.photos.append(inspectionPhoto)

inspectionPhoto = PhotoObject()
inspectionPhoto.inspector_id = 565
inspectionPhoto.photo_name = "My new Photo Object"
advert.object.object_data.premises.photos.append(inspectionPhoto)

#produce object
commandCenter.ProduceKafkaMessage(advert)